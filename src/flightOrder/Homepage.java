package flightOrder;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;




public class Homepage {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","D:\\selenium webdriver\\ChromeDriver\\chromedriver_win32\\chromedriver.exe");
				
		// mobile looks
		Map<String, String> mobileEmulation = new HashMap<>();
		mobileEmulation.put("deviceName", "Nexus 5");
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
		WebDriver driver = new ChromeDriver(chromeOptions);
		driver.manage().window().maximize();
		
		
		// Homepage
		driver.get("https://pegipegi.com/");
		driver.findElement(By.xpath("/html/body/div/div/div/div[2]/div[3]/div/div[1]/div[3]/a[2]")).click(); //Click flight Menu
		
		// Flight Homepage
		
		//Select Depature and Destination
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/div[1]/div[1]")).click();Thread.sleep(1000);// Click Depature 
		driver.findElement(By.className("box-input__input")).sendKeys("CGK"); Thread.sleep(1000);// Search CGK
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/div[2]/div[3]/div[16]/div[2]")).click(); Thread.sleep(1000); // Chosee CGK
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/div[2]/div[2]/div[2]/input")).sendKeys("UPG"); Thread.sleep(1000); // Search UPG
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/div[2]/div[3]/div[17]/div[2]")).click(); Thread.sleep(1000); //chosee UPG 
		
		//Select Depart date
		driver.findElement(By.className("pick-date")).click();Thread.sleep(1000); // Click Depature Date 
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/section/div[2]/div[1]/div/div[26]")).click();  //Chose date
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/section/div[1]/div[2]/div/div[2]/div[1]/div[2]")).click(); 
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/section/div[2]/div[2]/div/div[24]/div")).click();  // Choose return Date
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/section/div[3]/div[2]/button")).click();Thread.sleep(1000);// Click Simpan
		
		//Select passanger Number
		driver.findElement(By.className("pick-passenger")).click(); Thread.sleep(1000);//Click Passanger
		
		//Penumpang 3 Dewasa 2 Anak
		for( int i = 0; i <= 2; i++ ){
			driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/div[5]/div[2]/div[1]/div[2]/img[2]")).click();
			driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/div[5]/div[2]/div[2]/div[2]/img[2]")).click(); 
		}
		
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/div[5]/div[3]/button")).click();
		
		//Cari Pesawat
		driver.findElement(By.xpath("/html/body/div/div/div/div/div[2]/div/section[1]/div[1]/button")).click();
		
		
		Thread.sleep(3000);
		System.out.println(driver.getTitle());
		driver.quit();
		
	}

}
